<?php get_header(); ?>

<div class="banner-top bg-planos">

    <h1>Nossos planos</h1>

</div>

<?php include('nossosplanos.php'); ?>

<div class="mt-3 mt-md-5 pt-md-2">

<?php include('planos-especiais.php'); ?>

</div>

<?php get_footer(); ?>
