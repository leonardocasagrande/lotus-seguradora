<?php get_header() ?>

    <section class="banner-cotacao banner-top">
        <div class="container">
            <h2 class="text-white text-center title-cotacao">Cotação</h2>
        </div>
    </section>

<div class="container">
    <div class="row">
        <div class="col-md-6 py-4">
            <?php  include 'como-funciona.php'; ?>
        </div>

        <div class="col-md-6 d-flex align-items-center">
            <?php include 'box-form.php'; ?>
        </div>
    </div>
</div>

    <?php

        include 'vantagens.php';

    ?>


<div class="mt-5">

<?php

        include 'perguntas-frequentes.php';

    ?>

</div>

<?php get_footer() ?>
