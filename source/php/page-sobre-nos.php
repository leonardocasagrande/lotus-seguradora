<?php get_header(); ?>

<div class="banner-top bg-sobrenos">

    <h2>Sobre nós</h2>

</div>

<div class="container py-5 my-md-3 ">

    <div class="row align-items-center justify-content-center text-center text-md-left">

        <div class="col-md-6 d-flex justify-content-center my-3 my-md-0">

            <h1 class="color-blue font-weight-bold">

                A proteção <br>
                veicular que <br>
                você precisa <br>

            </h1>

        </div>

        <div class="col-md-6">

            <h3 class="color-orange font-weight-bold">Na Vide, VOCÊ TEM <br>
                GARANTIA DE PROTEÇÃO!</h3>

            <p>Para criar uma relação positiva com nossos associados, e mais competitiva em relação ao mercado, a Vide
                prioriza a qualidade e a agilidade no atendimento. Assim, oferecemos um atendimento personalizado para
                você se sentir mais tranquilo.</p>

        </div>

    </div>

</div>

<div class="d-flex flex-wrap text-center text-white text-md-left valores">

    <div class="col-md-6 bg-orange px-0 d-flex align-items-center justify-content-center">

        <div class="p-5 item">

            <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/binoculars.png" alt="Binoculos">

            <h2 class="font-weight-bold my-4">Visão</h2>

            <p class="pb-3">Criar serviços com abrangência nacional que ofereçam proteção e tranquilidade para pessoas e
                empresas.</p>

            <p>
                <b>

                    Comportamento ético; <br>
                    Defesa dos interesses dos associados; <br>
                    Atitudes criativas e inovadoras;<br>
                    Comunicação eficiente.

                </b>

            </p>

        </div>

    </div>

    <div class="col-md-6 bg-blue px-0 d-flex align-items-center justify-content-center">

        <div class="p-5 item">

            <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/pillar.png" alt="pillar">

            <h2 class="font-weight-bold my-4">Valores</h2>

            <p>Oferecer, aos associados, uma consultoria efetiva, utilizando práticas e soluções inovadoras que atendam
                às
                necessidades específicas, solucionando problemas e minimizando riscos, com um atendimento personalizado
                e
                diferenciado.</p>

        </div>


    </div>

</div>

<?php include('porque-lotus.php'); ?>

<?php include('vantagens.php'); ?>

<!-- <div class="detail-bggrey"></div> -->



<div class="mt-5">

    <?php include('perguntas-frequentes.php'); ?>

</div>

<?php get_footer(); ?>