<div class="bg-blue beneficios my-5 py-5">

    <div class="container">


        <div class="col-md-6 px-0">

            <h2 class="text-white text-center mb-4"><b>Benefícios</b></h2>


            <div class="row justify-content-md-start justify-content-center text-white">

                <div class="col-4 item">

                    <div class="box">

                        <div class="icon">

                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/thief.png"> <br>

                        </div>

                        Roubo

                    </div>

                </div>

                <div class="col-4 item">

                    <div class="box">

                        <div class="icon">

                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/rouber.png"> <br>

                        </div>

                       Furto



                    </div>

                </div>

                <div class="col-4 item">

                    <div class="box">

                        <div class="icon">

                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/crash.png"> <br>

                        </div>
                         Colisão

                    </div>

                </div>

                <div class="col-4 item">

                    <div class="box">

                        <div class="icon">

                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/car-t.png"> <br>

                        </div>
                        Fenômenos <br>
                        da natureza

                    </div>

                </div>

                <div class="col-4 item">

                    <div class="box">

                        <div class="icon">

                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/fall.png"> <br>

                        </div>
                        Indenização <br> Integral

                    </div>

                </div>

                <div class="col-4 item">

                    <div class="box">

                        <div class="icon">

                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/crashed.png"> <br>

                        </div>
                    Danos <br> a terceiros

                    </div>

                </div>

                <div class="col-4 item">

                    <div class="box">

                        <div class="icon">

                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/twoc.png"> <br>

                        </div>

                      Carro reserva

                    </div>

                </div>

                <div class="col-4 item">

                    <div class="box">

                        <div class="icon">

                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/doorcar.png"> <br>

                        </div>

                        Cobertura <br>
                        de vidros

                    </div>

                </div>

                <div class="col-4 item">

                    <div class="box">

                        <div class="icon">

                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/fire.png"> <br>

                        </div>

                        Incêndio

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>
