<div class="container beneficios my-5 py-md-5">

  <div class="col-md-6 px-0">

    <h2 class="color-blue text-center mb-4"><b>Na Vide você ainda tem!</b></h2>

    <div class="row justify-content-center color-black">

      <div class="col-4 item">

        <div class="box">

          <div class="icon">

            <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/car-resc.png"> <br>

          </div>

          Reboque <br>
          ilimitado (consulte)

        </div>

      </div>

      <div class="col-4 item">

        <div class="box">

          <div class="icon">

            <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/key.png"> <br>

          </div>

          Chaveiro

        </div>

      </div>

      <div class="col-4 item">

        <div class="box">

          <div class="icon">

            <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/way.png"> <br>

          </div>

          Motorista <br> substituto

        </div>

      </div>

      <!--            <div class="col-4 item">-->
      <!---->
      <!--                <div class="box">-->
      <!---->
      <!--                    <div class="icon">-->
      <!---->
      <!--                        <img src="--><?php //echo get_stylesheet_directory_uri(); 
                                                ?>
      <!--/dist/img/auction.png"> <br>-->
      <!---->
      <!--                    </div>-->
      <!---->
      <!--                    Assistência <br>-->
      <!--                    jurídica-->
      <!---->
      <!--                </div>-->
      <!---->
      <!--            </div>-->

      <div class="col-4 item">

        <div class="box">

          <div class="icon">

            <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/tombstone.png"> <br>

          </div>

          Assistência <br>
          funeral

        </div>

      </div>

      <div class="col-4 item">

        <div class="box">

          <div class="icon">

            <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/car-resc.png"> <br>

          </div>

          Transporte ao <br>
          veículo acidentado

        </div>

      </div>

      <div class="col-4 item">

        <div class="box">

          <div class="icon">

            <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/vib.png"> <br>

          </div>

          Instalação <br>
          de rastreador

        </div>

      </div>

      <div class="col-4 item">

        <div class="box">

          <div class="icon">

            <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/luggage.png"> <br>

          </div>

          Hospedagem <br>
          em hotel
        </div>

      </div>

      <div class="col-4 item">

        <div class="box">

          <div class="icon">

            <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/mainte.png"> <br>

          </div>

          Substituição do <br>
          pneu furado

        </div>

      </div>

      <div class="col-4 item">

        <div class="box">

          <div class="icon">

            <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/first-aid-kit.png"> <br>

          </div>

          Remoção médica <br>
          inter-hospitalar

        </div>

      </div>


      <div class="col-4 item">

        <div class="box">

          <div class="icon">

            <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/battery.png"> <br>

          </div>
          Socorro elétrico <br>
          e mecânico

        </div>

      </div>

      <!--            <div class="col-4 item">-->
      <!---->
      <!--                <div class="box">-->
      <!---->
      <!--                    <div class="icon">-->
      <!---->
      <!--                        <img src="--><?php //echo get_stylesheet_directory_uri(); 
                                                ?>
      <!--/dist/img/home.png"> <br>-->
      <!---->
      <!--                    </div>-->
      <!--                    Assistência <br>-->
      <!--                    residencial-->
      <!---->
      <!--                </div>-->
      <!---->
      <!--            </div>-->

      <div class="col-4 item">

        <div class="box">

          <div class="icon">

            <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/cab.png"> <br>

          </div>

          Táxi

        </div>

      </div>

      <!--            <div class="col-4 item">-->
      <!---->
      <!--                <div class="box">-->
      <!---->
      <!--                    <div class="icon">-->
      <!---->
      <!--                        <img src="--><?php //echo get_stylesheet_directory_uri(); 
                                                ?>
      <!--/dist/img/pet.png"> <br>-->
      <!---->
      <!--                    </div>-->
      <!---->
      <!--                    Assistência <br>-->
      <!--                    PET-->
      <!---->
      <!--                </div>-->
      <!---->
      <!--            </div>-->

      <div class="col-4 item">

        <div class="box">

          <div class="icon">

            <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/fuel-station.png"> <br>

          </div>

          Auxílio na falta <br>
          de combustível

        </div>

      </div>

    </div>

  </div>

</div>