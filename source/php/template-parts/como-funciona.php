<div class="como">

    <h2 class="font-weight-bold color-orange my-5 py-md-4 text-center text-md-left"><b>Como funciona?</b></h2>

    <div class="d-flex flex-wrap justify-content-center justify-content-md-start align-items-center">

        <div class="box">

            <div class="box-info">

                <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/data.png" alt="Ficha"> <br>

                <span class="color-orange"><b>1. <span
                                class="color-blue">Preencha os seus dados Pessoais</span></b></span>

            </div>

        </div>

        <div class="box">

            <div class="box-info">

                <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/acess.png" alt="Acesso"> <br>

                <span class="color-orange"><b>2. <span class="color-blue">
    Selecione os itens que deseja incluir na proteção
                        </span></b></span>
            </div>
        </div>

        <div class="box">

            <div class="box-info">

                <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/paper-plane.png" alt="Paper plan">
                <br>

                <span class="color-orange"><b>3. <span
                                class="color-blue">Envie a sua solicitação totalmente online</span></b></span>
            </div>
        </div>

        <div class="box">

            <div class="box-info">

                <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/professor-consultation.png"
                     alt="Consultoria"> <br>

                <span class="color-orange"><b>4. <span
                                class="color-blue">Aguarde o retorno de um dos nossos consultores</span></b></span>


            </div>
        </div>

        <div class="box">

            <div class="box-info">

                <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/happiness.png"
                     alt="Contrate"> <br>

                <span class="color-orange"><b>5. <span class="color-blue">Contrate a
sua Proteção!</span></b></span>
            </div>
        </div>

    </div>


</div>