<?php

/* Disable WordPress Admin Bar for all users but admins. */
show_admin_bar(false);

(add_theme_support('post-thumbnails'));


function replace_core_jquery_version() {
    wp_deregister_script( 'jquery' );
    // Change the URL if you want to load a local copy of jQuery from your own server.
    wp_register_script( 'jquery', "https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js", array(), '3.4.1' );
}
add_action( 'wp_enqueue_scripts', 'replace_core_jquery_version' );


// Our custom post type function
function create_posttype() {

    register_post_type( 'pacotes',
        // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Pacotes' ),
                'singular_name' => __( 'Pacotes' )
            ),
            'public' => true,
            'has_archive' => false,
            'rewrite' => array('slug' => 'pacotes'),

        )
    );
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );


/**
 * Remove the slug from published post permalinks. Only affect our CPT though.
 */
function wpex_remove_cpt_slug( $post_link, $post, $leavename ) {
    if ( 'pacotes' != $post->post_type || 'publish' != $post->post_status ) {
        return $post_link;
    }
    $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );
    return $post_link;
}
add_filter( 'post_type_link', 'wpex_remove_cpt_slug', 10, 3 );


/**
 * Some hackery to have WordPress match postname to any of our public post types
 * All of our public post types can have /post-name/ as the slug, so they better be unique across all posts
 * Typically core only accounts for posts and pages where the slug is /post-name/
 */
function wpex_parse_request_tricksy( $query ) {
    // Only noop the main query
    if ( ! $query->is_main_query() )
        return;
    // Only noop our very specific rewrite rule match
    if ( 2 != count( $query->query ) || ! isset( $query->query['page'] ) ) {
        return;
    }
    // 'name' will be set if post permalinks are just post_name, otherwise the page rule will match
    if ( ! empty( $query->query['name'] ) ) {
        $query->set( 'post_type', array( 'post', 'pacotes' ) );
    }
}
add_action( 'pre_get_posts', 'wpex_parse_request_tricksy' );
?>
