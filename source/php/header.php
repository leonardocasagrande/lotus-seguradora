<!DOCTYPE html>

<html lang="pt_BR">

<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-226392404-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-226392404-1');
	</script>


	<meta charset="UTF-8">

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>

		<?php wp_title(''); ?>

	</title>

	<meta name="robots" content="index, follow" />

	<meta name="msapplication-TileColor" content="#ffffff">

	<meta name="theme-color" content="#ffffff">

	<?php wp_head(); ?>

</head>

<body>
	<div class="loading">

		<div class="svg-loading">

			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin:auto;background:#fff;display:block;" width="200px" height="200px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
				<path d="M10 50A40 40 0 0 0 90 50A40 42 0 0 1 10 50" fill="#f39219" stroke="none" transform="rotate(312.902 50 51)">
					<animateTransform attributeName="transform" type="rotate" dur="1s" repeatCount="indefinite" keyTimes="0;1" values="0 50 51;360 50 51"></animateTransform>
				</path>
			</svg>

		</div>

	</div>
	<style>
		.loading {
			position: absolute;
			width: 100%;
			height: 100%;
			z-index: 9999999;
			background-color: #fff;
		}

		.svg-loading {
			z-index: 9999999999;
			position: fixed;
			height: 13em;
			width: 13em;
			overflow: show;
			margin: auto;
			top: 0;
			left: 0;
			bottom: 0;
			right: 0;
		}
	</style>

	<link rel="preload" href="<?= get_stylesheet_directory_uri(); ?>/dist/css/style.css" as="style" onload="this.onload=null;this.rel='stylesheet'">

	<link rel="stylesheet" href="<?= get_stylesheet_directory_uri(); ?>/dist/css/style.css">

	<script>
		gtag('config', 'UA-137270184-1');
	</script>

	<header class="py-md-4 py-2 bg-vide">

		<div class="d-flex flex-wrap align-items-center justify-content-center">

			<div class="col-xl-2 col-6 text-center d-xl-none">

				<a href="<?php echo get_site_url(); ?>/">

					<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/logovide.png" alt="Logo Vide Clube de Benefícios">

				</a>

			</div>

			<div class="d-xl-none col-6  text-right" id="open-menu">

				<div class="consts float-right" onclick="this.classList.toggle('active')">
					<svg xmlns="" width="80" height="80" viewBox="0 0 200 200">
						<g stroke-width="6.5" stroke-linecap="round">
							<path d="M72 82.286h28.75" fill="#009100" fill-rule="evenodd" stroke="#fff" />
							<path d="M100.75 103.714l72.482-.143c.043 39.398-32.284 71.434-72.16 71.434-39.878 0-72.204-32.036-72.204-71.554" fill="none" stroke="#fff" />
							<path d="M72 125.143h28.75" fill="#009100" fill-rule="evenodd" stroke="#fff" />
							<path d="M100.75 103.714l-71.908-.143c.026-39.638 32.352-71.674 72.23-71.674 39.876 0 72.203 32.036 72.203 71.554" fill="none" stroke="#fff" />
							<path d="M100.75 82.286h28.75" fill="#009100" fill-rule="evenodd" stroke="#fff" />
							<path d="M100.75 125.143h28.75" fill="#009100" fill-rule="evenodd" stroke="#fff" />
						</g>
					</svg>
				</div>

			</div>

			<div id="box-menu" class="col-xl-12  d-none d-xl-flex align-items-center justify-content-center  text-center">

				<a href="<?= get_site_url(); ?>/" class="mr-5 d-none d-xl-block">

					<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/logovide.png" alt="Logo Vide Clube de Benefícios">

				</a>

				<div class="dropdown">

					<a class="dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Proteção Auto
					</a>

					<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

						<a class="dropdown-item" href="<?php echo get_site_url(); ?>/protecao-auto/">Vide Auto pop</a>

						<a class="dropdown-item" href="<?php echo get_site_url(); ?>/duas-rodas/">Vide Duas Rodas</a>

						<a class="dropdown-item" href="<?php echo get_site_url(); ?>/auto-carga/">Vide Auto Carga</a>

						<a class="dropdown-item" href="<?php echo get_site_url(); ?>/auto-import/">Vide Auto Import</a>

						<a class="dropdown-item" href="<?php echo get_site_url(); ?>/vans-e-utilitarios/">Vide Vans e Utilitários</a>

						<!-- <a class="dropdown-item" href="<?php echo get_site_url(); ?>/auto-franquia/">Auto Franquia</a> -->

					</div>

				</div>

				<div class="dropdown">


					<a class="dropdown-toggle" id="protecoes" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Proteções especiais
					</a>

					<div class="dropdown-menu" aria-labelledby="protecoes">
						<a class="dropdown-item" href="<?php echo get_site_url(); ?>/veiculos-rebaixados">Veículos rebaixados</a>

						<a class="dropdown-item" href="<?php echo get_site_url(); ?>/off-road">OFF-Road</a>
						<a class="dropdown-item" href="<?php echo get_site_url(); ?>/taxi">Táxi</a>

						<a class="dropdown-item" href="<?php echo get_site_url(); ?>/motorista-de-app">Motorista de APP</a>

						<a class="dropdown-item" href="<?php echo get_site_url(); ?>/assistencia-funeral">Assistência Funeral</a>

					</div>

				</div>


				<div class="dropdown">

					<a class="dropdown-toggle" id="alotus" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">A Vide</a>

					<div class="dropdown-menu" aria-labelledby="alotus">
						<a class="dropdown-item" href="<?php echo get_site_url(); ?>/sobre-nos">Sobre nós</a>

						<a class="dropdown-item" href="<?php echo get_site_url(); ?>/nossos-fundadores">Fundadores</a>


					</div>


				</div>


				<a href="seja-um-consultor">
					Seja um consultor
				</a>


				<div class="dropdown">


					<a class="dropdown-toggle" id="area" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Área do consultor
					</a>

					<div class="dropdown-menu" aria-labelledby="area">
						<a class="dropdown-item" rel="noopener" target="_blank" href="http://calculo.videbeneficios.com.br/">Cálculo
							QAR</a>


						<a class="dropdown-item" rel="noopener" href="https://orion.hinova.com.br/sga/sgav4_lotus_cb/v5/login.php" target="_blank">SGA/MCV</a>


					</div>

				</div>


				<a href="cotacao" class="bg-orange text-white py-3 py-xl-4 px-3 rounded font-weight-bold">
					FAÇA SUA COTAÇÃO
				</a>


				<div class="box-redes d-xl-none d-flex justify-content-center">

					<a href="https://www.instagram.com/lotusbeneficios/">

						<i class="fab fa-instagram"></i>

					</a>


					<a href="https://www.facebook.com/lotusclubedebeneficios/">

						<i class="fab fa-facebook-f"></i>

					</a>

				</div>

			</div>

		</div>

	</header>