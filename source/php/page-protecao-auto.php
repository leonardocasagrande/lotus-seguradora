<?php get_header(); ?>

<section class="banner-auto-pop banner-top ">

    <div class="container">
        <div class="row align-items-center text-center text-md-left">
            <div class="col-md-6">
                <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/car-icon.png"
                     alt="Assistência Funeral">

                <h1 class="my-3"><b>Proteção Auto</b></h1>

                <p>Proteção completa, com o melhor custo-benefício do mercado! Agora você pode ter o seu veículo
                    protegido e ter mais tranquilidade no dia a dia.</p>

                <div class="box-price text-white">

                    <div class="pb-4">

                        <span>A PARTIR DE</span><br>

                        <span class="font-weight-bold">R$ <span class="value">99,90 </span></span>

                        <span>/MÊS</span>

                    </div>

                </div>

            </div>

            <div class="col-md-6 d-flex justify-content-center">

                <?php include 'box-form.php'; ?>

            </div>

        </div>

    </div>

</section>


<div class="container">

    <div class="row">

        <div class="col-md-6">

            <?php include('como-funciona.php'); ?>

        </div>

    </div>

</div>

<?php

include('beneficios.php');

include('ainda-tem.php');

?>

<?php get_footer(); ?>
