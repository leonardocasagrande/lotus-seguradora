if ($(window).width() >= 1200) {
  $(".carousel-mobile").removeClass("owl-planos");

  $(document).ready(function () {
    var fixme = $(".fixme");

    var fixmebottom = $("#myDiv").offset().top;

    var fixmeTop = fixme.offset().top;

    $(window).scroll(function () {
      var currentScroll = $(window).scrollTop();

      if (currentScroll >= fixmebottom) {
        fixme.css({
          position: "absolute",
        });
      } else if (currentScroll >= fixmeTop && currentScroll <= fixmebottom) {
        fixme.css({
          position: "fixed",

          top: "50px",
        });
      } else {
        fixme.css({
          position: "static",
        });
      }
    });
  });
}

$(".owl-vantagens").owlCarousel({
  loop: false,
  margin: 50,
  nav: true,
  autoplay: false,
  autoplayTimeout: 5000,
  autoplayHoverPause: true,
  responsive: {
    0: {
      items: 1,
    },
    1000: {
      items: 2,
    },
  },
});

$(".owl-planos").owlCarousel({
  loop: false,
  margin: 50,
  nav: true,
  autoplay: false,
  autoplayTimeout: 5000,
  autoplayHoverPause: true,
  responsive: {
    0: {
      items: 1,
    },
    1000: {
      items: 2,
    },
  },
});

var divs = $(".owl-nav, .owl-dots");

for (var i = 0; i < divs.length; i += 2) {
  divs
    .slice(i, i + 2)
    .wrapAll(
      "<div class='d-flex container nav justify-content-between flex-row-reverse align-items-center'</div>"
    );
}

$(".btn-pop, .close").click(function () {
  var form = $(".box-form");

  if (form.hasClass("active-form") === true) {
    form.removeClass("active-form");
  } else {
    form.addClass("active-form");
  }
});

// new WOW().init();

var boxMenu = $("#box-menu");
$("#open-menu").click(function () {
  console.log("clicousabosta");

  if (boxMenu.hasClass("active")) {
    boxMenu.fadeOut();

    boxMenu.removeClass("active");
  } else {
    boxMenu.fadeIn();

    boxMenu.addClass("active");

    boxMenu.removeClass("d-none");
  }
});

$(".cpf").mask("000.000.000-00");

$(".celphone")
  .mask("(99) 99999-9999")
  .focusout(function (event) {
    var target, phone, element;
    target = event.currentTarget ? event.currentTarget : event.srcElement;
    phone = target.value.replace(/\D/g, "");
    element = $(target);
    element.unmask();
    if (phone.length > 10) {
      element.mask("(99) 99999-99999");
    } else {
      element.mask("(99) 9999-99999");
    }
  });

var popup = $(".popup");

$(document).ready(function () {
  if (!document.cookie) {
    popup.attr("style", "display:flex");
    $(".closePopup").click(function (e) {
      e.preventDefault();
      popup.attr("style", "display:none");
      document.cookie = "popup=true";
    });
  }
});
