<div class="container plans"><h2 class="col-12 text-center color-blue font-weight-bold my-5 py-lg-4">Conheça nossos planos</h2><div class="carousel-mobile owl-carousel owl-planos d-md-flex flex-wrap justify-content-md-around"><div class="card item" style="width: 25rem;"><div class="card-body text-center"><div class="bg-orange rounded-circle d-inline-block p-4"><img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/car-icon.png" alt="Ícone proteção automovél"></div><h3 class="font-weight-bold color-orange mt-4">Proteção Auto</h3><p>Proteção completa, com o melhor custo-benefício do mercado! Agora você pode ter o seu veículo protegido e ter mais tranquilidade no dia a dia.</p><div class="box-price"><div class="pb-4"><span class="color-black">A PARTIR DE</span><br><span class="color-orange font-weight-bold">R$ <span class="value">99</span></span><span class="color-black">/MÊS</span></div><a href="/protecao-auto/" class="bg-orange py-2 text-white d-flex w-100 font-weight-bold justify-content-center w-100">CONTRATE</a></div></div></div><!-- <div class="card item" style="width: 20rem;">

            <div class="card-body text-center">

                <div class="bg-sblue rounded-circle d-inline-block p-4">

                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/franchising-icon.png"
                         alt="Ícone Auto Franquia">

                </div>

                <h3 class="font-weight-bold color-sblue mt-4">Auto Franquia</h3>

                <p>

                    Para quem já possui um seguro tradicional, mas está preocupado(a) com o alto valor da franquia, essa
                    é a melhor solução para você!

                </p>

                <div class="box-price">

                    <div class="pb-4">

                        <span class="color-black"> A PARTIR DE </span> <br>

                        <span class="color-sblue font-weight-bold">R$ <span class="value">15</span></span><span
                                class="color-black">/MÊS</span>

                    </div>

                    <a href="/auto-franquia/"
                       class="bg-sblue py-2 text-white d-flex w-100 font-weight-bold justify-content-center w-100">CONTRATE</a>

                </div>

            </div>
        </div> --><div class="card item" style="width: 25rem;"><div class="card-body text-center"><div class="bg-green rounded-circle d-inline-block p-4"><img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/img/rip-icon.png" alt="Ícone proteção automovél"></div><h3 class="font-weight-bold color-green mt-4">Assistência Funeral</h3><p>Toda a assessoria e segurança para proporcionar mais serenidade e simplificar este difícil momento para os nossos entes queridos.</p><div class="box-price"><div class="pb-4"><span class="color-black">A PARTIR DE</span><br><span class="color-green font-weight-bold">R$ <span class="value">19</span></span><span class="color-black">/MÊS</span></div><a href="/assistencia-funeral/" class="bg-green py-2 text-white d-flex w-100 font-weight-bold justify-content-center w-100">CONTRATE</a></div></div></div></div></div>