<?php get_header(); ?> <div class="bg-orange"><div class="container"><div class="row align-items-center py-3 py-md-5"><div class="text-white col-md-7 text-center text-md-left"><span class="font-weight-bold">Vide</span><h1 class="font-weight-bold"><?= the_title(); ?></h1><img class="my-2" src="<?= the_field('imagem') ?>"><div class="box-price text-white"> <?php if (get_field('preco') == true) { ?> <div class="pb-4"><span>A PARTIR DE</span><br><span class="font-weight-bold">R$ <span class="value"><?= get_field('preco') ?></span></span><span>/MÊS</span></div> <?php } ?> </div><div class="w-md-75"><p><b><?= the_field('descricao') ?></b></p></div></div><div class="col-md-5 position-relative"> <?php include('box-form.php'); ?> </div></div></div></div><div class="container"><div class="row"><div class="col-md-6"> <?php include('como-funciona.php'); ?> </div></div></div> <?php

include('beneficios.php');

include('ainda-tem.php');

?> <?php get_footer(); ?>