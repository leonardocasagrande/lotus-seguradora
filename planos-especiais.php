<div class="my-5 planos-especiais"><h2 class="color-orange mb-md-5 font-weight-bold text-center">Planos Especiais</h2><div class="d-flex special flex-wrap align-items-start carousel-mobile owl-carousel justify-content-center owl-planos mt-md-5 pt-md-4"> <?php
        $loop = new WP_Query(
            array(
                'post_type' => 'pacotes',
                'posts_per_page' => -1
            )
        );
        ?> <?php while ($loop->have_posts()) : $loop->the_post(); ?> <div class="col-md-3 m-auto col-11 text-center rounded item"><div class="bg-box"><img src="<?= the_field('imagem') ?>" alt=""></div><div class="box"><div class="container"><span class="color-lgrey">Vide</span><h3 class="font-weight-bold color-odd"><?= the_title() ?> </h3><div class="box-price"><div class="pb-4"> <?php if (get_field('preco') == true) { ?> <span class="color-black">A PARTIR DE</span><br><span class="color-odd font-weight-bold">R$ <span class="value"><?= the_field('preco') ?></span></span><span class="color-black">/MÊS</span> <?php } ?> </div><a href="<?= get_permalink(); ?>" class="bg-odd py-2 text-white d-flex w-100 font-weight-bold justify-content-center w-100">FAÇA SUA COTAÇÃO</a></div></div></div></div> <?php endwhile;
        wp_reset_query(); ?> </div></div>